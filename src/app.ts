import { Container, AsyncContainerModule, interfaces } from "inversify";
import "reflect-metadata"
import { InversifyExpressServer } from "inversify-express-utils";
import { TIPO } from "./constants/tipos";
import "./controller/ProdutoController";
import { ProdutoService } from "./service/ProdutoService";
import { ProdutoValidator } from "./validator/ProdutoValidator";
import { ProdutoRepository } from "./repository/ProdutoRepository";
import { createConnection, Connection } from "typeorm";
import bodyParser = require("body-parser");



export class App{
    private container: Container;
    private server: InversifyExpressServer;
    private serverApp: any;
    
    constructor(){
        this.container = new Container();        
    }

    public iniciar(conexao: Connection){
        this.iniciarConexao(conexao);
        this.iniciarInject();
        this.iniciarServer();
    }

    private iniciarConexao(conexao: Connection){
        const repositorio = conexao.getCustomRepository(ProdutoRepository);
        this.container.bind<ProdutoRepository>(TIPO.Repository).toConstantValue(repositorio);

    }

    private iniciarInject(){
        this.container.bind<ProdutoValidator>(TIPO.Validator).to(ProdutoValidator);
        this.container.bind<ProdutoService>(TIPO.Service).to(ProdutoService);    
    }

    private iniciarServer(){
        this.server = new InversifyExpressServer(this.container);
        this.server.setConfig((app) => {
            // add body parser
            app.use(bodyParser.urlencoded({
                extended: true
            }));
            app.use(bodyParser.json());
        });

        this.trataErros();
        
        this.serverApp = this.server.build();

    }

    private trataErros(){
        this.server.setErrorConfig((app) => {
            app.use((err, req, res, next) => {
                console.error(err.stack);
                res.status(500).send(JSON.parse('{"error":"'+err.message+'"}'));
                
            });
        });
    }

    public executar(){
        this.serverApp.listen(3100, () => {
            console.log("Express application is up and running on port 3100")
        });        
    }
}