import { Produto } from "../entity/Produto";
import { rejects } from "assert";
import { ProdutoRepository } from "../repository/ProdutoRepository";
import { TIPO } from "../constants/tipos";
import { inject, injectable } from "inversify";
import { ProdutoValidator } from "../validator/ProdutoValidator";

@injectable()
export class ProdutoService{

    @inject(TIPO.Repository)
    repositorio: ProdutoRepository;

    @inject(TIPO.Validator)
    validator: ProdutoValidator;


    public inserir(produto: Produto): Promise<Produto>{

        this.validator.validaCampos(produto);
        return this.repositorio.inserir(produto);        
    }

    public listar():Promise<Produto[]>{
        return this.repositorio.listar();        
    }

    public buscarPorId(id: number):Promise<Produto>{
        this.validator.validaId(id);
        return new Promise<Produto>((resolve, reject) => {
            this.repositorio.buscarPorId(id)
                .then(produto => {
                    console.log(produto);
                    if(!produto) reject("Produto não encontrado");
                    resolve(produto);
                })
                .catch(error => {
                    console.log(error);
                    reject(error);
                });
          });

        //Falta tratar a promise para ver se o produto retornado não foi vazio - Criar Validador!
        //return this.repositorio.buscarPorId(id);
    }

    public atualizar(id: number, produto: Produto):Promise<Produto>{
        this.validator.validaId(id);
        console.log(produto);
        return this.repositorio.atualizar(id,produto);
    }

    public deletar(id: number):Promise<Produto>{
        this.validator.validaId(id);

        return this.repositorio.deletar(id); 
    }
}