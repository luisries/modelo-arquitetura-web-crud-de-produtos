export const TIPO = {
    Repository: Symbol("Repository"),
    Validator: Symbol("Validator"),
    Service: Symbol("Service")
};