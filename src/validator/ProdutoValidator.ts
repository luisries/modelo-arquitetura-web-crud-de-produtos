import { injectable } from "inversify";
import { Produto } from "../entity/Produto";

@injectable()
export class ProdutoValidator{
    public validaProduto(produto: Produto){
        if(!produto) throw new Error("Entidade produto inválida!");
    }

    public validaCampos(produto: Produto){
        this.validaProduto(produto);
        if(!produto.nome || produto.nome.length < 3) throw new Error("Nome obrigatório!");
        if(!produto.descricao || produto.descricao.length < 3) throw new Error("Descrição obrigatória!");
    }

    public validaId(id: number){
        if(!id || isNaN(id) || id <= 0) throw new Error("Id Inválido");
    }
}