import {EntityRepository, Repository, EntityManager, DeepPartial} from "typeorm";
import {Produto} from "../entity/Produto";

export class CRUDGenericRepository<T> extends Repository<T>
{   
    public inserir(entidade: DeepPartial<T>):Promise<T>{
        const entidadeCriada = this.create(entidade);
        const entidadeNova = this.manager.save(entidadeCriada);
        return (entidadeNova);
    }

    public listar():Promise<T[]>{
        return this.find();        
    }

    public buscarPorId(id: number):Promise<T>{
        return this.findOne(id);                
    }

    public atualizar(id: number, entidade: DeepPartial<T>):Promise<T>{
        this.update(id, entidade);
        return this.findOne(id);
        
    }

    public deletar(id: number):Promise<T>{
        return this.findOne(id).then(
            produtoRemover => this.remove(produtoRemover)
        );        
    }
}