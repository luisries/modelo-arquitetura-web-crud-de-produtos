import {EntityRepository, Repository} from "typeorm";
import {Produto} from "../entity/Produto";
import { CRUDGenericRepository } from "./CRUDGenericRepository";
import { injectable, inject } from "inversify";



@EntityRepository(Produto)
@injectable()
export class ProdutoRepository extends CRUDGenericRepository<Produto>
{   
}
