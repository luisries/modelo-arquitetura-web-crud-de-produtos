import { App } from "./app";
import { createConnection } from "typeorm";

const app = new App();


createConnection().then(async connection => {
    app.iniciar(connection);
    app.executar();
}).catch((err)=> console.log("TypeORM connection error: ", err));