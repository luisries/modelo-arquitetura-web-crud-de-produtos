import {
    controller,
    httpGet,
    httpPost,
    response,
    requestParam,
    requestBody,
    httpDelete,
    httpPut
} from "inversify-express-utils";
import { TIPO } from "../constants/tipos";
import { inject } from "inversify";
import { ProdutoService } from "../service/ProdutoService";
import { Request, Response } from "express";
import { Produto } from "../entity/Produto";

@controller("/api/v1/produtos")
export class ProdutoController{
    constructor(@inject(TIPO.Service) private produtoService: ProdutoService){}

    @httpGet('/')
    private async listar(req: Request, res: Response) {
        const produtos = await this.produtoService.listar();
        res.send(produtos);
    }

    @httpPost('/')
    private async inserir(req: Request, res: Response) {
        const produto = await this.produtoService.inserir(req.body);
        res.status(201).send(produto);  

    }

    @httpGet('/:id')
    private async buscar(req: Request, res: Response) {
        const produto = await this.produtoService.buscarPorId(req.params.id);
        res.send(produto);
    }


    @httpDelete('/:id')
    private async deletar(req: Request, res: Response) {
        const produto = await this.produtoService.deletar(req.params.id);
        res.send(produto);        
    }


    @httpPut('/:id')
    private async atualizar(req: Request, res: Response) {
        let produtoAtualizar = Object.assign(new Produto(), req.body);

        const produto = await this.produtoService.atualizar(req.params.id, produtoAtualizar);
        res.send(produto);  
    }
}